<?php

use App\Http\Controllers\auth\TokenController;
use App\Http\Controllers\FileCreateController;
use App\Http\Controllers\FileDeleteController;
use App\Http\Controllers\FileIndexController;
use App\Http\Controllers\FileUpdateController;
use App\Http\Controllers\task\CompletedTaskController;
use App\Http\Controllers\task\CreateController;
use App\Http\Controllers\task\DeleteController;
use App\Http\Controllers\task\SearchTaskController;
use App\Http\Controllers\task\UpdateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    dd(1);
});

Route::post('/token', TokenController::class);

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/task/create', CreateController::class);
    Route::put('/task/{task}', UpdateController::class);
    Route::delete('/task/{task}', DeleteController::class);
    Route::put('/completedTask/{task}', CompletedTaskController::class);
    Route::get('/searchTask', SearchTaskController::class);
});

