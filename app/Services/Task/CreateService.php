<?php


namespace App\Services\Task;

use App\DTO\Task\TaskDTO;
use App\Repositories\TaskRepository;
use InvalidArgumentException;

class CreateService extends BaseService
{
    public function __construct(private TaskRepository $taskRepository)
    {
    }

    public function create($request): TaskDTO
    {
        $task = $this->taskRepository->create($request);

        if (!$task) {
            throw new InvalidArgumentException('something went wrong');
        }

        return $this->makeDTO($task);
    }
}
