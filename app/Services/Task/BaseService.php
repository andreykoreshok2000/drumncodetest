<?php


namespace App\Services\Task;


use App\DTO\Task\TaskDTO;
use App\Models\Task;

class BaseService
{
    /**
     * @param Task $task
     * @return TaskDTO
     */
    public function makeDTO(Task $task): TaskDTO
    {
        return new TaskDTO(
            $task->id,
            $task->user_id,
            $task->status,
            $task->priority,
            $task->title,
            $task->description,
            $task->parent_id,
            $task->completed_at,
            $task->updated_at,
            $task->created_at
        );
    }
}
