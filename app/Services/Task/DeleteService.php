<?php


namespace App\Services\Task;


use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

class DeleteService
{
    public function __construct(private TaskRepository $taskRepository)
    {
    }

    public function delete(Task $task): JsonResponse
    {

        if (is_array($task)) {
            return response()->json($task, 422);
        }

        return response()->json($this->taskRepository->delete($task));
    }

}
