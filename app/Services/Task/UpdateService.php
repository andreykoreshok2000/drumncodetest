<?php


namespace App\Services\Task;


use App\DTO\Task\TaskDTO;
use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use InvalidArgumentException;

class UpdateService extends BaseService
{
    public function __construct(private TaskRepository $taskRepository)
    {
    }

    public function update(Model $task, Request $request): TaskDTO|JsonResponse
    {
        $task = $this->taskRepository->update($task, $request);

        if (!$task instanceof Task) {
            return response()->json($task, 422);
        }

        return response()->json($this->makeDTO($task));
    }
}
