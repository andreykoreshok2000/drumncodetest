<?php


namespace App\Services\Task;

use App\DTO\Task\TaskDTO;
use App\Http\Requests\Task\SearchTaskRequest;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;
use InvalidArgumentException;

class SearchTaskService extends BaseService
{

    public function __construct(private TaskRepository $taskRepository)
    {
    }

    public function search(SearchTaskRequest $request)
    {
        $data = $this->taskRepository->search($request);

        if (!$data) {
            throw new InvalidArgumentException('something went wrong');
        }

        $parents = [];
        foreach ($data as $task) {
            if (is_null($task->parent_id)) {
                $parents[0][$task->id] = $this->makeDTO($task);
            }else{
                $parents[$task->parent_id][$task->id] = $this->makeDTO($task);
            }
        }

        $treeElem = $parents[0];

        $this->taskRepository->makeTree($treeElem, $parents);

        return $treeElem;
    }
}
