<?php


namespace App\Services\Task;

use App\DTO\Task\TaskDTO;
use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use InvalidArgumentException;

class CompletedTask extends BaseService
{

    public function __construct(private TaskRepository $taskRepository)
    {
    }

    public function completedTask(Task $task): JsonResponse
    {
        $task = $this->taskRepository->completedTask($task);

        if (!$task instanceof Task) {
            return response()->json($task, 422);
        }

        return response()->json($this->makeDTO($task));
    }
}
