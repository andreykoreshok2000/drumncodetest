<?php


namespace App\DTO\Task;


class TaskDTO
{

    public int $id;
    public int $user_id;
    public string $status;
    public int $priority;
    public string $title;
    public string $description;
    public ?int $parent_id;
    public ?string $completed_at;
    public string $updated_at;
    public string $created_at;

    /**
     * TaskDTO constructor.
     * @param int $id
     * @param int $user_id
     * @param string $status
     * @param int $priority
     * @param string $title
     * @param string $description
     * @param ?int $parent_id
     * @param ?string $completed_at
     * @param string $updated_at
     * @param string $created_at
     */
    public function __construct(
        int $id,
        int $user_id,
        string $status,
        int $priority,
        string $title,
        string $description,
        ?int $parent_id,
        ?string $completed_at,
        string $updated_at,
        string $created_at,
    ){
        $this->id = $id;
        $this->user_id = $user_id;
        $this->status = $status;
        $this->priority = $priority;
        $this->title = $title;
        $this->description = $description;
        $this->parent_id = $parent_id;
        $this->completed_at = $completed_at;
        $this->updated_at = $updated_at;
        $this->created_at = $created_at;
    }
}
