<?php


namespace App\Repositories;

use App\Models\Task;
use App\Repositories\interfaceRepository\TaskRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Enums\ServerStatus;

class TaskRepository implements TaskRepositoryInterface
{

    /**
     * create task
     * @param $request
     * @return Task
     */
    public function create($request): Task
    {
        return Task::create($request)->refresh();
    }

    /**
     * @param Task $task
     * @param Request $request
     * @return Task|JsonResponse
     */
    public function update(Task $task, Request $request): Task|array
    {
        if ($this->checkTaskStatusDone($task)) {
            return ['user' =>
                'the user cannot completed the task if the children of the task do not have the completed status'
            ];
        }

        $task->fill($request->except('user_id'))->save();

        return $task;
    }

    /**
     * @param Task $task
     * @return bool|array|null
     */
    public function delete(Task $task): bool|array|null
    {
        if ($task->status === ServerStatus::DONE->value) {
            return ['user' => 'the user cannot delete completed tasks'];
        }

        return $task->delete();
    }

    /**
     * @param Task $task
     * @return Task|array
     */
    public function completedTask(Task $task): Task|array
    {
        if ($this->checkTaskStatusDone($task)) {
            return ['user' => 'the user cannot completed the task
            if the children of the task do not have the completed status'];
        }

        $task->status = ServerStatus::DONE->value;
        $task->completed_at = date('Y-m-d H:i:s');
        $task->save();

        return $task;
    }

    /**
     * @param Request $request
     * @return Collection|array
     */
    public function search(Request $request): Collection|array
    {
        $data = Task::query();

        $data->where('user_id', $request->user()->id);

        if ($request->has('status')) {
            $data->where('status', $request->input('status'));
        }

        if ($request->has('priority')) {
            $data->where('priority', $request->input('priority'));
        }

        if ($request->has('title')) {
            $data->where('title', $request->input('title'));
        }

        if ($request->has('description')) {
            $data->where('description', $request->input('description'));
        }

        if ($request->has('sort')) {
            foreach ($request->input('sort') as $column => $sortType) {
                $data->orderBy($column, $sortType);
            }
        }

        return $data->get();
    }

    /**
     * make tree for the tasks
     * @param $treeElem
     * @param $parents
     */
    public function makeTree($treeElem, $parents)
    {
        foreach ($treeElem as $key => $item) {
            if (!isset($item->children)) {
                $item->children = [];
            }

            if (array_key_exists($key, $parents)) {
                $item->children = $parents[$key];
                $this->makeTree($item->children, $parents);
            }
        }
    }

    /**
     * @param Task $task
     * @return bool
     */
    private function checkTaskStatusDone(Task $task): bool
    {
        $childTask = Task::where('parent_id', $task->id)->first();

        if (!is_null($childTask)) {
            if ($childTask->status === ServerStatus::DONE->value) {
                $this->checkTaskStatusDone($childTask);
            }else{
                return true;
            }
        }

        return false;
    }

}
