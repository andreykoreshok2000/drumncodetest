<?php


namespace App\Repositories\interfaceRepository;


use App\Models\Task;
use Illuminate\Http\Request;

interface TaskRepositoryInterface
{
    public function create(Request $request);
    public function update(Task $task, Request $request);
    public function delete(Task $task);
}
