<?php


namespace App\Enums;

enum ServerStatus: string {
    case TODO = 'todo';
    case DONE = 'done';
}
