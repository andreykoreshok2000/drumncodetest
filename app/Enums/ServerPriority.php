<?php


namespace App\Enums;

enum ServerPriority: string {
    case CRITICAL = '1';
    case HIGH_PRIORITY = '2';
    case NEUTRAL = '3';
    case LOW_PRIORITY = '4';
    case UNKNOWN = '5';
}
