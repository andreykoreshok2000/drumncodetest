<?php

namespace App\Http\Requests\Task;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use App\Enums\ServerStatus;
use App\Enums\ServerPriority;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can('update', $this->task);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // parent id is not equal to current id
        return [
            'status' => ['required', 'string', Rule::enum(ServerStatus::class)],
            'priority' => ['required', 'string', Rule::enum(ServerPriority::class)],
            'title' => 'required|string',
            'description' => 'required|string',
            'completed_at' => 'date_format:Y-m-d H:i:s',
            'parent_id' => 'integer|exists:App\Models\Task,id'
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return void
     *
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new HttpResponseException(response()->json([
            'errors' => $validator->errors()
        ], 400)));
    }
}
