<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Enums\ServerStatus;
use App\Enums\ServerPriority;

class SearchTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'status' => ['string', Rule::enum(ServerStatus::class)],
            'priority' => ['string', Rule::enum(ServerPriority::class)],
            'title' => 'string',
            'description' => 'string',
            'completed_at' => 'date_format:Y-m-d H:i:s',
            'sort' => ['array', 'min:1', 'array:completed_at,created_at,status,priority'],
            'sort.*' => Rule::in(['desc', 'asc'])
        ];
    }
}
