<?php

namespace App\Http\Controllers\task;

use App\DTO\Task\TaskDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Task\CreateTaskRequest;
use App\Services\Task\CreateService;
use Illuminate\Http\JsonResponse;

class CreateController extends Controller
{
    public function __construct(private CreateService $taskService)
    {}

    public function __invoke(CreateTaskRequest $request): TaskDTO|JsonResponse
    {
        return response()->json($this->taskService->create($request->validated()));
    }
}
