<?php

namespace App\Http\Controllers\task;

use App\Http\Controllers\Controller;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Models\Task;
use App\Services\Task\UpdateService;
use Illuminate\Http\JsonResponse;

class UpdateController extends Controller
{
    public function __construct(private UpdateService $taskService)
    {}

    public function __invoke(Task $task, UpdateTaskRequest $request): JsonResponse
    {
        return $this->taskService->update($task, $request);
    }
}
