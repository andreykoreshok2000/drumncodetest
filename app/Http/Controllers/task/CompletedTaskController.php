<?php

namespace App\Http\Controllers\task;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Services\Task\CompletedTask;
use Illuminate\Http\JsonResponse;

class CompletedTaskController extends Controller
{
    public function __construct(private CompletedTask $taskService)
    {}

    public function __invoke(Task $task): JsonResponse
    {
        return $this->taskService->completedTask($task);
    }
}
