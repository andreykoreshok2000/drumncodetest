<?php

namespace App\Http\Controllers\task;

use App\Http\Controllers\Controller;
use App\Http\Requests\Task\SearchTaskRequest;
use App\Services\Task\SearchTaskService;
use Illuminate\Http\JsonResponse;

class  SearchTaskController extends Controller
{
    public function __construct(private SearchTaskService $taskService)
    {}

    public function __invoke(SearchTaskRequest $request): JsonResponse
    {
        return response()->json($this->taskService->search($request));
    }
}
