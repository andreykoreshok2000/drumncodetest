<?php

namespace App\Http\Controllers\task;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Services\Task\DeleteService;
use Illuminate\Http\JsonResponse;

class DeleteController extends Controller
{
    public function __construct(private DeleteService $taskService)
    {}

    public function __invoke(Task $task): JsonResponse|bool
    {
        $this->authorize('delete', $task);

        return $this->taskService->delete($task);
    }
}
