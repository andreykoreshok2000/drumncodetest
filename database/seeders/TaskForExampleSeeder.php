<?php

namespace Database\Seeders;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Enums\ServerStatus;
use App\Enums\ServerPriority;

class TaskForExampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = Task::create([
            'user_id' => User::first()->id,
            'status' => ServerStatus::TODO->value,
            'priority' => ServerPriority::CRITICAL->value,
            'title' => Str::random(10),
            'description' => Str::random(),
            'completed_at' => date('Y-m-d H:i:s')
        ]);

        for ($i = 1; $i <= 10; $i++) {
            Task::create([
                'user_id' => User::first()->id,
                'status' => ServerStatus::TODO->value,
                'priority' => ServerPriority::CRITICAL->value,
                'title' => Str::random(10),
                'description' => Str::random(),
                'completed_at' => date('Y-m-d H:i:s'),
                'parent_id' => $task->id
            ]);
        }
    }
}
